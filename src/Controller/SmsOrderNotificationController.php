<?php

namespace Drupal\commerce_simplesms\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for mocean_sms_broadcast pages.
 */
class SmsOrderNotificationController extends ControllerBase {
	
  public function smsOrderNotificationContent() {
    $build = [];
    $build['mocean_sms_notification_settings_form'] = $this->formBuilder()->getForm('Drupal\commerce_simplesms\Form\SmsOrderNotificationContentForm');
    return $build;
  }
  
}
