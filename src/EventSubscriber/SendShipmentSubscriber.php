<?php

namespace Drupal\commerce_simplesms\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\commerce_simplesms\Utility;

/**
 *
 */
class SendShipmentSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = ['commerce_shipment.ship.post_transition' => 'onSend'];

	  return $events;
  }

  /**
   *
   */
  public function onSend(WorkflowTransitionEvent $event) {
    $content = \Drupal::config('commerce_simplesms.content');
    $enable = $content->get('shipped_enable');
    $message =  $content->get('shipped_content');
	  $order = $event->getEntity();

    /** @var Utility $utility */
    $utility = \Drupal::service('commerce_simplesms.utility');

    $recipient    = $order->getOrder()->getCustomer()->get($utility->getFieldName())->value;
    $store_name   = $order->getOrder()->getStore()->getName();
    $store_email  = $order->getOrder()->getStore()->getEmail();
    $order_number = $order->getOrder()->getOrderNumber();
    $order_track  = $order->getTrackingCode();

	  if (!empty($recipient) && $enable) {
      $placeholder = ['{store_name}', '{store_email}', '{order_number}', '{order_track_code}'];
      $replace = [$store_name, $store_email, $order_number, $order_track];
      $message = str_replace($placeholder, $replace, $message);
      \Drupal::logger('commerce_simplesms')->notice($message);

	    $result = $utility->smsOrderNotificationSendMessage($recipient, $message);

      if (!$result) {
        \Drupal::logger('commerce_simplesms')
          ->error('Notification SMS could not be sent for order number @number', ['@number' => $order_number]);
      }
    }
  }
}
