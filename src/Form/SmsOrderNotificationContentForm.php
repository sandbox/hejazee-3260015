<?php

namespace Drupal\commerce_simplesms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class SmsOrderNotificationContentForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_simplesms_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $content = $this->config('commerce_simplesms.content');

	  $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Manage content of SMS to be sent on shipping events.'),
    ];

    $form['content'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-publication',
    ];

    $form['ready'] = [
      '#type' => 'details',
      '#title' => $this->t('Ready for shipping'),
      '#group' => 'content',
	    '#tree' => TRUE,
    ];

    $form['ready']['ready_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
	    '#default_value' => $content->get('ready_enable'),
    ];

	  $form['ready']['ready_content'] = [
      '#type' => 'textarea',
	    '#cols'  => 60,
      '#rows'  => 5,
      '#title' => $this->t('Content'),
	    '#default_value' => $content->get('ready_content'),
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {order_track_code}'),
    ];

    $form['shipped'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipped'),
      '#group' => 'content',
	    '#tree' => TRUE,
    ];

	  $form['shipped']['shipped_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
	    '#default_value' => $content->get('shipped_enable'),
    ];

    $form['shipped']['shipped_content'] = [
      '#type' => 'textarea',
	    '#cols'  => 60,
      '#rows'  => 5,
      '#title' => $this->t('Content'),
	    '#default_value' => $content->get('shipped_content'),
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {order_track_code}'),
    ];

	  return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$this->config('commerce_simplesms.content')
      ->set('ready_content', $form_state->getValue(['ready','ready_content']))
      ->set('ready_enable', $form_state->getValue(['ready','ready_enable']))
      ->set('shipped_content', $form_state->getValue(['shipped','shipped_content']))
      ->set('shipped_enable', $form_state->getValue(['shipped','shipped_enable']))
      ->save();

    $this->messenger()->addStatus($this->t('Content saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_simplesms.content'];
  }

}
