<?php

namespace Drupal\commerce_simplesms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_simplesms\Utility;

/**
 * Provides a form for MoceanSMS settings.
 */
class SmsOrderNotificationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_simplesms_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('commerce_simplesms.settings');

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
    ];

    $form['settings']['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine Name of Telephone Field'),
	    '#description' => $this->t('Check for Manage Fields in Account Settings.'),
      '#default_value' => $settings->get('field_name'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_simplesms.settings')
      ->set('field_name', $form_state->getValue('field_name'))
      ->save();

    $this->messenger()->addStatus($this->t('Settings saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_simplesms.settings'];
  }

}
