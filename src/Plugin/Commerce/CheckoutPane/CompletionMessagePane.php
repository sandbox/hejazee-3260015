<?php

namespace Drupal\commerce_simplesms\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_simplesms\Utility;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a completion SMS pane.
 *
 * @CommerceCheckoutPane(
 *   id = "checkout_pane_complete_order",
 *   label = @Translation("Completion SMS"),
 *   default_step = "_disabled",
 * )
 */
class CompletionMessagePane extends CheckoutPaneBase {


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'content' => 'Your order with order number {order_number} and total charge of {total_price} has been placed at {store_name}.',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    return $this->t('Content: @content', ['@content' => $this->configuration['content']]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['content'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMS Content'),
      '#default_value' => $this->configuration['content'],
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {total_price}'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['content'] = $values['content'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $message =  $this->configuration['content'];

    /** @var Utility $utility */
    $utility = \Drupal::service('commerce_simplesms.utility');

	  $recipient    = $this->order->getCustomer()->get($utility->getFieldName())->value;
    $store_name   = $this->order->getStore()->getName();
    $store_email  = $this->order->getStore()->getEmail();
    $order_number = $this->order->getOrderNumber();
    $total_price  = $this->order->getTotalPrice();

	  if ($recipient) {
      $placeholder = ['{store_name}', '{store_email}', '{order_number}', '{total_price}'];
      $replace = [$store_name, $store_email, $order_number, $total_price];
      $message = str_replace($placeholder, $replace, $message);

	    $result = $utility->smsOrderNotificationSendMessage($recipient, $message);

      if (!$result) {
        \Drupal::logger('commerce_simplesms')
          ->error('Notification SMS could not be sent for order number @number', ['@number' => $order_number]);
      }
    }

    return $pane_form;
  }
}
