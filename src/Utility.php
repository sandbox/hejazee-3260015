<?php

namespace Drupal\commerce_simplesms;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sms\Direction;
use Drupal\sms\Exception\RecipientRouteException;
use Drupal\sms\Message\SmsMessage;
use Drupal\sms\Provider\SmsProviderInterface;

/**
 *
 */
class Utility {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * @var \Drupal\sms\Provider\SmsProviderInterface
   */
  protected SmsProviderInterface $provider;

  /**
   *
   */
  public function __construct(SmsProviderInterface $provider) {
    $this->provider = $provider;
  }

  /**
  * SMS API.
  */
  public function smsOrderNotificationSendMessage($recipient, $message) {
    $sms = new SmsMessage();
    $sms->setMessage($message);
    $sms->addRecipient($recipient);
    $sms->setDirection(Direction::OUTGOING);

    try {
      $this->provider->queue($sms);

      return TRUE;
    }
    catch (RecipientRouteException $e) {
      // Thrown if no gateway could be determined for the message.
      $this->messenger()->addError($this->t('No gateway could be determined for sending SMS message.'));
      watchdog_exception('commerce_simplesms', $e);

      return FALSE;
    }
    catch (\Exception $e) {
      // Other exceptions can be thrown.
      $this->messenger()->addError($this->t('Error sending SMS.'));
      watchdog_exception('commerce_simplesms', $e);

      return FALSE;
    }
  }

 /**
  * Fetch field name for telephone.
  */
  public function getFieldName() {
    $settings = \Drupal::config('commerce_simplesms.settings');
	  return $settings->get('field_name');
  }
}
